import React, { Component } from "react";

export default class ItemShoe extends Component {
  render() {
    let { image, name, price } = this.props.shoe;
    return (
      <div className="col-3">
        <div className="card mb-3">
          <img src={image} className="card-img-top" alt="..." />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <strong>
              <p
                style={{ fontSize: "20px" }}
                className="card-text mt-2 mb-2 text-danger"
              >
                {price}$
              </p>
            </strong>
            <a
              onClick={() => {
                this.props.handleAddToCart(this.props.shoe);
              }}
              style={{ fontWeight: "bold", border: "3px solid black" }}
              href="#"
              className="btn btn-light"
            >
              Add To Cart
            </a>
          </div>
        </div>
      </div>
    );
  }
}
