import React, { Component } from "react";

export default class CartShoe extends Component {
  renderTbody = () => {
    return this.props.cart.map((item, index) => {
      return (
        <tr key={index} style={{ lineHeight: "3.5" }}>
          <td>
            <strong>{item.id}</strong>
          </td>
          <td>
            <strong> {item.name}</strong>
          </td>
          <td>
            <strong> {item.price * item.soLuong}</strong>
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handleChange(item.id, -1);
              }}
              className="btn btn-warning"
            >
              <strong> -</strong>
            </button>
            <strong className="pr-2 pl-2"> {item.soLuong}</strong>
            <button
              onClick={() => {
                this.props.handleChange(item.id, +1);
              }}
              className="btn btn-danger"
            >
              +
            </button>
          </td>
          <td>
            <img style={{ width: 50 }} src={item.image} alt="" />
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handleDelete(item.id);
              }}
              className="btn btn-success"
            >
              Remove
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Price</th>
              <th>Quantity</th>
              <th>Image</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>{this.renderTbody()}</tbody>
        </table>
      </div>
    );
  }
}
